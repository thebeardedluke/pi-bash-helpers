#!/bin/bash

source /etc/profile

################################################################################
#
# Debugging
#
#  0 - Silent
#  1 - Info (getting/setting/init)
#  2 - Debug Detail

decho () {
  if [[ ${DEBUG} -ge ${1} ]]; then
    if [[ ${1} -eq 1 ]]; then
      echo -n "INFO:  "
    elif [[ ${1} -ge 2 ]]; then
      echo -n "DEBUG: "
    fi
    echo "${2}"
  fi
}

################################################################################
#
# Status Functions
# 

# GpioIsEnabled
#----------------------------------------
GpioIsEnabled() {
  if [[ -z ${1} ]]; then
    decho 2 "GpioIsEnabled: No GPIO port passed"
    return
  fi

  if [[ -e /sys/class/gpio/gpio${1} ]]; then
    return 0
  else
    return 1
  fi
}


################################################################################
#
# Init Functions
#

# GpioEnable
#----------------------------------------
GpioEnable() {
  if [[ -z ${1} ]]; then
    decho 2 "No GPIO port passed"
    return -1
  fi
  
  if [[ -e /sys/class/gpio/gpio${1} ]]; then
    decho 1 "GPIO ${1} is already set up, skipping"
  else 
    echo "${1}" > /sys/class/gpio/export
    sleep 0.1
    decho 1 "GPIO ${1} is now set up"
  fi
}

################################################################################
#
# Setting Functions
#

# GpioSetDirection
#----------------------------------------
GpioSetDirection() {
  if [[ -z ${1} ]]; then
    decho 2 'Port was not supplied'
    return -1
  fi
  
  if [[ -z ${2} ]]; then
    decho 2 'Port direction was not supplied'
    return -1
  fi

  if [[ ! ${2} == 'out' && ! ${2} == 'in' ]]; then
    decho 2 'Port direction was not correct'
    return -1
  fi
  
  if ! GpioIsEnabled ${1}; then
    decho 2 "ERROR: GPIO ${1} is not set up"
    return -1
  fi
  
  CURDIR=$(</sys/class/gpio/gpio${1}/direction)

  if [[ ! ${CURDIR} == "${2}" ]]; then
    decho 1 "Setting port ${1} direction to '${2}'"
    echo "${2}" > /sys/class/gpio/gpio${1}/direction
  else
    decho 1 "Port ${1} direction is already '${2}'"
  fi
}

# GpioSetState
#----------------------------------------
GpioSetState() {
  if [[ -z ${1} ]]; then
    decho 2 'Port was not supplied'
    return -1
  fi

  if [[ -z ${2} ]]; then
    decho 2 'Port state was not supplied'
    return -1
  fi

  if [[ ! ${2} -eq 0 && ! ${2} -eq 1 ]]; then
    decho 2 'Port direction was not correct'
    return -1
  fi

  if ! GpioIsEnabled ${1}; then
    decho 2 "ERROR: GPIO ${1} is not set up"
    return -1
  fi

  decho 1 "Setting port ${1} value to '${2}'"
  echo "${2}" > /sys/class/gpio/gpio${1}/value
}

################################################################################
#
# Getting Functions
#

# GpioGetDirection
#----------------------------------------
GpioGetDirection() {
  if [[ -z ${1} ]]; then
    decho 2 'Port was not supplied'
    return -1
  fi

  if ! GpioIsEnabled ${1}; then
    decho 2 "ERROR: GPIO ${1} is not set up"
    return -1
  fi
  
  RET=`cat /sys/class/gpio/gpio${1}/direction`
  
  decho 2 "GPIO ${1} direction is ${RET}"
  return ${RET}
}

# GpioGetState
#----------------------------------------
GpioGetState() {
  if [[ -z ${1} ]]; then
    decho 2 'Port was not supplied'
    return -1
  fi

  if ! GpioIsEnabled ${1}; then
    decho 2 "ERROR: GPIO ${1} is not set up"
    return -1
  fi

  RET=`cat /sys/class/gpio/gpio${1}/value`
  
  decho 2 "GPIO ${1} value is ${RET}"
  return ${RET}
}
