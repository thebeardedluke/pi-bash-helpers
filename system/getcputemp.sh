#!/bin/bash

source /etc/profile

which bc > /dev/null 2>&1

if [[ ${?} -gt 0 ]]; then
  echo "This tool requires bc"
  exit 1
fi

TEMPC=`vcgencmd measure_temp | cut -d '=' -f 2 | cut -d "'" -f 1`
TEMPF=`echo "(${TEMPC}*9/5)+32" | bc`

echo ${TEMPF}
