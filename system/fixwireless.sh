#!/bin/bash

source /etc/profile

IP=`wpa_cli -i wlan0 status | grep ip_address | cut -d '=' -f 2 | cut -d '.' -f 1,2`

if [[ ${IP} == "169.254" ]]; then
  wpa_action wlan0 reload
  echo "`date`: Reloaded wifi"
fi

# Fin